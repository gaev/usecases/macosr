#!/bin/bash

echo "Downloading Xquartz"
curl -OL https://dl.bintray.com/xquartz/downloads/XQuartz-2.7.11.dmg

hdiutil attach XQuartz-2.7.11.dmg
sudo installer -package /Volume/XQuartz-2.7.11/XQuartz.pkg -target /
hdiutil detach /Volumes/XQuartz-2.7.11/
rm XQuartz-2.7.11.dmg
