#!/bin/bash

# Download and install gitlab-runner
sudo curl --output /usr/local/bin/gitlab-runner https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-darwin-amd64
sudo chmod +x /usr/local/bin/gitlab-runner

# add gitlab serveur in know host
mkdir -p ~/.ssh/ || true
ssh-keyscan -t ecdsa -H gitlab.paca.inrae.fr >> ~/.ssh/known_hosts
