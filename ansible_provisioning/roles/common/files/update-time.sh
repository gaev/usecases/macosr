#/bin/bash

echo "Time synchronization forcing..."
sudo touch /var/db/ntp-kod
sudo chmod 666 /var/db/ntp-kod

# force time update
sudo sntp -sS pool.ntp.org

# force time cron 1m
(sudo crontab -l 2>/dev/null;  echo "*/1 * * * * sntp -sS pool.ntp.org") | sudo crontab -

# never know...
#sudo echo "server pool.ntp.org minpoll 4 maxpoll 5" > /etc/ntp.conf

